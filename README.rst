Tinkerer
========

This is a fork of the original `tinkerer
<https://github.com/vladris/tinkerer>`_ blogging engine. An effort to fix some
bugs and update the engine to keep compatibility with the latest `sphinx
<http://www.sphinx-doc.org/en/master/>`_ release.

What is Tinkerer?
-----------------

Tinkerer is a blogging engine/static website generator powered by Sphinx.

It allows blogging in reStructuredText format, comes with out-of-the-box 
support for post publishing dates, authors, categories, tags, post archive,
RSS feed generation, comments powered by Disqus and more.

Tinkerer is also highly customizable through Sphinx extensions.

Why Tinkerer?
-------------

* Because *"hacker"* is way overused nowadays.
* Because static websites are cool.
* Because you already write documentation in RST format and you love it.
* Because you have Pygments for sharing highlighted code.
* Because you can use your favorite Sphinx extensions right away.

How to Tinker?
--------------

To install tinkerer from this repository:

#. Clone the repository to your computer::

    git clone https://gitlab.com/geriochoa/pytinkerer.git 

#. Change directory to the project's folder::

    cd pytinkerer

#. Run the installation script::

    python setup.py install

Skim through the `Documentation <http://tinkerer.me/pages/documentation.html>`_.

